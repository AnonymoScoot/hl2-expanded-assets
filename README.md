# HL2 Expanded Assets

Half-Life 2 Expanded Assets is a modification that contains many custom made assets including models and textures for use by other modifications.

## Mounting the assets

Other modifications can list thid mod as a dependency. To use the content of this mod, add this line to your gameinfo.txt file of your modification as a search path:
```
game+mod    "|gameinfo_path|../HLExpanded/"
```
This will mount all assets for your mod to use. You can then build maps with Source Engine's mapping tool Hammer with all the new assets available to be used.

Make sure to clearly state the version of this mod being used as a dependency so others downloading your modification can know which version they should use.
